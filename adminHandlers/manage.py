import tornado.web
import json
import math
from database.database import DatabaseAdmin
from handlers.admin import auth

db = DatabaseAdmin()

class ManageArticles(tornado.web.RequestHandler):
    def get(self,page):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            limit=(int(page)-1)*10
            articles = db.executeFetch("SELECT  * FROM tbl_articles limit 10 offset "+str(limit))
            if not articles:
                self.render("error.html" , error=" !!هیچ مقاله ای ثبت نشده است")
            else:
                pages =  db.executeFetch(" SELECT COUNT(*) FROM tbl_articles")
                page_count = math.ceil( (int(pages[0][0])) / 10 )
                self.render("admin/manage.html",items = articles , page_count=page_count)
        else:
            self.redirect("/admin")
class DeleteArticle(tornado.web.RequestHandler):
    def post(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            article_id = tornado.escape.xhtml_escape(self.get_argument('id'))
            try:
                db.execute("DELETE FROM tbl_comments WHERE article_id = %s"%(article_id))
                print("delete comment done")
                db.execute("DELETE FROM tbl_articles WHERE id = %s"%(article_id))
                print("DElete ARticle done")
                self.write(json.dumps({'success':'true'}))
            except Exception as DbException:
                self.write(json.dumps({'success':'false','error':'خطا در حذف مطلب'}))
        else:
            self.redirect("/admin")