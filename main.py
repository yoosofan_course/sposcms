import tornado.web
import tornado.ioloop
import os
from handlers.article import *
from handlers.sendComment import SendComment
from handlers.admin import *
from handlers.main import *
from handlers.adminHandlers import *
from adminHandlers.manage import ManageArticles
from adminHandlers.manage import DeleteArticle

def make_app():
  settings = {
      "static_path": os.path.join(os.path.dirname(__file__), "statics"),
      "template_path": os.path.join(os.path.dirname(__file__), "templates"),
      "cookie_secret": "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",  
      "login_url": "/login",        
      "xsrf_cookies": False,
      "debug":True,
    }
  return tornado.web.Application([
      (r"/admin", AdminLogin),
      (r"/admin/index", AdminIndex),
      (r"/logout", LogoutIndex),
      (r"/admin/addArticle",AddArticle),
      (r"/admin/EditArticle/([0-9]+)",EditArticle),
      (r"/admin/loadcm",LoadComment),
      (r"/admin/DeleteComment",DeleteComment),
      (r"/page/([0-9]+)",MainArticle),
      (r"/search/([\w]+)/([\w]+)",SearchArticle),# moshkele html
      (r"/search",SearchArticle),
      (r"/article/([0-9]+)",ArticleHandler),
      (r"/sendcomment",SendComment),
      (r"/admin/manage/([0-9]+)",ManageArticles),
      (r"/admin/manage",ManageArticles),
      (r"/admin/deleteArticle",DeleteArticle),

  ],**settings);

if __name__ == "__main__":
  app = make_app()
  app.listen(8888)
  tornado.ioloop.IOLoop.current().start()
