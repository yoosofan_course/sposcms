import tornado.web
from database.database import DatabaseVisitor
from tornado.escape import *
from handlers.admin import auth
import math

database = DatabaseVisitor()

class MainArticle(tornado.web.RequestHandler):
    def get(self,page_id):#page_id az url miyad
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            limit = (int(page_id)-1) * 10 # page_id hamoon shomare safhe mishe ke masalan dar safhe 2 biyad az 11 ta 20 maghale ro neshoon bede
            article = database.executeFetch("SELECT * FROM tbl_articles limit 10 offset "+ str(limit))
            if not article :
                self.render("error.html" , error="این صفحه موجود نمی باشد !!")
            else:
                counts = database.executeFetch("SELECT COUNT(*) FROM tbl_articles")
                count = math.ceil( (int(counts[0][0])) / 10 )
                self.render("main.html",articles=article,page_id = int(page_id),count=count)
        else:
            self.redirect("/admin")

class SearchArticle(tornado.web.RequestHandler):
    def get(self,textforsearch,check_key):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            if str(check_key) == "text":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE text LIKE %s" ,["%"+textforsearch+"%"])
                self.render("search.html",articles=article)
                
            if str(check_key) == "title":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE title LIKE %s" ,["%"+textforsearch+"%"])
                self.render("search.html",articles=article)

            if str(check_key) == "keywords":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE keywords LIKE %s" ,["%"+textforsearch+"%"])
                self.render("search.html",articles=article)

            if not article :
                self.render("error.html" , error="این مقاله موجود نیست !!")

        else :
            self.redirect("/admin")

    def post(self):
        textforsearch = str(self.get_argument("texts"))
        check_key = self.get_argument("checkbox")
        page_id = int(self.get_argument("page_id"))
        if(page_id==0):
            page_id = 1 # baraye offset gozashtan
            self.set_cookie("texts",textforsearch)
            self.set_cookie("checkbox",check_key)
        limit = (int(page_id)-1) * 10
        if (len(textforsearch) < 1):#baraye inke agar len text 0 bashe miyad kolle database ro barmigardoone
            self.write(json.dumps({"error":"len"}))
        else :
            if str(check_key) == "text":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE text LIKE %s limit 10 offset "+ str(limit) ,["%"+textforsearch+"%"])
                counts = database.executeFetch("SELECT COUNT(*) FROM tbl_articles WHERE text LIKE %s" ,["%"+textforsearch+"%"])
                page_count = math.ceil( (int(counts[0][0])) / 10 )

            if str(check_key) == "title":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE title LIKE %s limit 10 offset "+ str(limit) ,["%"+textforsearch+"%"])
                counts = database.executeFetch("SELECT COUNT(*) FROM tbl_articles WHERE title LIKE %s" ,["%"+textforsearch+"%"])
                page_count = math.ceil( (int(counts[0][0])) / 10 )

            if str(check_key) == "keywords":
                article = database.executeFetch("SELECT * FROM tbl_articles WHERE keywords LIKE %s limit 10 offset "+ str(limit) ,["%"+textforsearch+"%"])
                counts = database.executeFetch("SELECT COUNT(*) FROM tbl_articles WHERE keywords LIKE %s" ,["%"+textforsearch+"%"])
                page_count = math.ceil( (int(counts[0][0])) / 10 )

            if not article :
                self.write(json.dumps({"error":1}))
            else:
                self.write(json.dumps({"article":article , "page_count":page_count}))
        self.finish()