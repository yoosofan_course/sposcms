import tornado.web
from database.database import *
from tornado.escape import *
from handlers.admin import auth

database = DatabaseAdmin()
        
class AddArticle(tornado.web.RequestHandler):
    def get(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            self.render("admin/addArticle.html")  
        else:
            self.redirect("/admin")

    def post(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            title = tornado.escape.xhtml_escape(self.get_argument('subject'))
            desc = tornado.escape.xhtml_escape(self.get_argument('desc'))
            text = tornado.escape.xhtml_escape(self.get_argument('text'))
            keywords = tornado.escape.xhtml_escape(self.get_argument('keywords'))
            try:
                SQL_INSERT = "INSERT INTO tbl_articles(title,keywords,description,text) VALUES (%s,%s,%s,%s)"
                database.execute(SQL_INSERT,(title,keywords,desc,text))            
                self.write(json.dumps({'add':True}))
            except Exception as err:
                 self.write(json.dumps({'add':False}))
        else:
            self.redirect("/admin")

class EditArticle(tornado.web.RequestHandler):
    def get(self,article_id):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            article = database.executeFetch("SELECT * FROM tbl_articles WHERE id =%s"% article_id)
            if not article : 
                self.render("error.html" , error="این مقاله موجود نمیباشد !!")
            self.render("admin/EditArticle.html" ,article = article[0])
        else:
            self.redirect("/admin")
            
    def post(self,article_id):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            title = tornado.escape.xhtml_escape(self.get_argument('subject'))
            desc = tornado.escape.xhtml_escape(self.get_argument('desc'))
            text = tornado.escape.xhtml_escape(self.get_argument('text'))
            keywords = tornado.escape.xhtml_escape(self.get_argument('keywords'))
            SQL_INSERT = "UPDATE tbl_articles SET title=%s,keywords =%s,description=%s,text=%s WHERE id=%s"
            try:
                database.execute(SQL_INSERT,(title,keywords,desc,text,article_id))           
                self.write(json.dumps({'edit':True}))
            except Exception as error:
                 self.write(json.dumps({'edit':False}))
        else:
            self.redirect("/admin")

class LoadComment(tornado.web.RequestHandler):
    def get(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            comments = database.executeFetch("SELECT * from tbl_comments limit 10 offset 0");
            self.render("loadcm.html",comments = comments)
        else:
            self.redirect("/admin")

    def post(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            article_id = self.get_argument("article_id")           
            counter = int(self.get_argument("counter"))
            limit = counter * 10
            comments = database.executeFetch("SELECT id,name,text,article_id FROM tbl_comments WHERE article_id = %s limit 10 offset "+str(limit) ,[article_id])
            count_comment = database.executeFetch("SELECT COUNT(*) FROM tbl_comments WHERE article_id = %s ",[article_id])
            self.write(json.dumps({"comments":comments , "limit" : limit+10 , "count_comment" : count_comment[0][0]}))
        else:
            self.redirect("/admin")

class DeleteComment(tornado.web.RequestHandler):
    def post(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        if ret_val:
            idn = self.get_argument("id")
            try:
                database.execute("DELETE from tbl_comments WHERE id=%s"% idn)
                self.write(json.dumps({'delete':1}))
            except Exception as err:
                self.write(json.dumps({'delete':0}))
        else:
            self.redirect("/admin")
