import tornado.web
import json
from database.database import DatabaseVisitor
db = DatabaseVisitor();

class SendComment(tornado.web.RequestHandler):
    def post(self):
        name = tornado.escape.xhtml_escape(self.get_argument('Name')) # in xhtm ro bayad bezarim vase sql inject
        email = tornado.escape.xhtml_escape(self.get_argument('Email'))
        text = tornado.escape.xhtml_escape(self.get_argument('Text'))
        article_id = tornado.escape.xhtml_escape(self.get_argument('id'))
        if(len(name) == 0 or len(email) == 0 or len(text) == 0):
            self.write(json.dumps({'success':0}))
        else:
            try:
                db.execute("INSERT INTO tbl_comments(name,mail,text,article_id) VALUES(%s,%s,%s,%s)",(name,email,text,article_id))
                self.write(json.dumps({'success':'true','name':name,'text':text}))
            except Exception as Error:
                print(Error)
                self.write(json.dumps({'error':1}))
                
            