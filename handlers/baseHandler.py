import tornado.web
class BaseHandler(tornado.web.RequestHandler):
  def send_error(self,status_code=500, **kwargs):
    self.render("error.html",error = status_code)
  def write_error(self,status_code=404,**kwargs):
    self.render("error.html",error=status_code)
    