import tornado.ioloop
import tornado.web
import random
import string
import hashlib
import json
from database.database import DatabaseAdmin

dbobj = DatabaseAdmin()

def generatecookie(length):
    s=string.ascii_lowercase+string.digits+string.ascii_uppercase
    return ''.join(random.sample(s,length))

class AdminLogin(tornado.web.RequestHandler):
    def get(self):  
        cookie = self.get_cookie("lg")
        if cookie == None :
            self.render("admin.html")
        else:
            self.redirect("/admin/index")
    def post(self):
        usrck = tornado.escape.xhtml_escape(self.get_argument('username'))
        pasck = tornado.escape.xhtml_escape(self.get_argument('password'))
        firstmd5 = hashlib.md5(str(pasck).encode('utf-8')).hexdigest() #THIS IS AN ERROR
        salt = dbobj.executeFetch("SELECT salt FROM tbl_users WHERE username=%s" ,[usrck])
        if salt :
            firstmd5 = firstmd5 + str(salt[0][0])
            secondmd5 = hashlib.md5(str(firstmd5).encode('utf-8')).hexdigest()
            if dbobj.executeFetch("SELECT username FROM tbl_users WHERE username=%s AND password = %s" ,[usrck,secondmd5]):
                cookie = generatecookie(45)
                self.set_cookie("lg",cookie)
                dbobj.execute("UPDATE tbl_users SET cookie=%s WHERE username=%s" ,[cookie,usrck])
                self.write(json.dumps({'login':1}))
            else:
                self.write(json.dumps({'login':0}))
        else :
            self.write(json.dumps({'login':0}))
            
class AdminIndex(tornado.web.RequestHandler):
    def get(self):
        ret_val = auth().checklogin(self.get_cookie("lg"))
        user=ret_val
        if ret_val:
            self.render("login_index.html",user=user)
        else:
            self.redirect("/admin")
class LogoutIndex(tornado.web.RequestHandler):
    def get(self):
            self.clear_cookie("lg")
            self.redirect("/admin")

class auth():
    def checklogin(self,cookie):
        if cookie != None :
            usr = dbobj.executeFetch("SELECT username FROM tbl_users WHERE cookie=%s" ,[cookie] )
            if usr:
                return usr[0][0]
            else:
                return False