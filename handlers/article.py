import tornado.web 
import tornado.escape
from database.database import DatabaseVisitor
from handlers.baseHandler import BaseHandler
import json
db = DatabaseVisitor()

class ArticleHandler(tornado.web.RequestHandler):
      def get(self,article_id):
            article = db.executeFetch("SELECT * FROM tbl_articles WHERE id = %s" ,[article_id])
            comments = db.executeFetch("SELECT name,text FROM tbl_comments WHERE article_id = %s limit 10 offset 0" ,[article_id])
            if not article :
                  self.render("error.html" , error="این مقاله موجود نمیباشد !!")
            else:
                  self.render("article.html" , article = article[0] , comments = comments , article_id=article_id)

      def post(self,article_id):
            article_id = self.get_argument("article_id")
            counter = int(self.get_argument("counter"))
            limit = counter * 10
            comments = db.executeFetch("SELECT name,text FROM tbl_comments WHERE article_id = %s limit 10 offset "+str(limit) ,[article_id])
            count_comment = db.executeFetch("SELECT COUNT(*) FROM tbl_comments WHERE article_id = %s ",[article_id])
            self.write(json.dumps({"comments":comments , "limit" : limit+10 , "count_comment" : count_comment[0][0]}))
            #limit+10 mifrestim chon biyad jolotar hesab kone ke bad ke did comment nist biyad load more ro bebande vase inke ma roosh nazanim bad biyad check kone bebine comment nis bad bebande