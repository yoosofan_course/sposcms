Simple readme



سلام. قانون‌های زیر را رعایت کنید


۱. هیچ بسته ی آماده‌ی دیگری به کار نبرید. در پروژه‌ی اصلی فقط از یک ابزار دیگر استفاده شده است و آن هم فعلا فقط دز یک جای خاص. پس


<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">


را به کار نبرید.


۲. برنامه‌ی نهایی باید روز مرورگرهای جدیدی مانند firefox و chrome و opera پاسخ دهد. سازگاری با مرورگرهای قدیمی و به ویژه Internet Explorer به هیچ عنوان هدف نیست. محصول نهایی درست است که تحت وب است ولی برای افراد ویژه‌ای به کار برده می شود و فقط کاربرد خاص دارد. بنابراین معنای تحت وب بودن به شکلی که روی شبکه‌ی جهانی باشد نیست.


۳. پرونده‌های اضافی مانند .pyc را حذف کنید.


۴. برای نوشتن توضیح‌ها از restructred text کمک بگیرید و البته باید آن را تا اندازه‌ای یاد بگیرید. به نوعی مستندسازی و نوشتن توضیح‌ها به طور کامل با این روش بسیار ساده انجام می‌شود و به هیچ عنوان ابزارهایی مانند office از هیچ نوعی به کار برده نمی‌شود. برای یادگیری آن می‌توانید سایت‌های زیرا بررسی کنید.

*  `<http://rest-sphinx-memo.readthedocs.org/en/latest/ReST.html>`_
*  `Markdown and reStructuredText <https://gist.github.com/dupuy/1855764>`_
*  `<http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html>`_
*  `table of content <http://docutils.sourceforge.net/docs/ref/rst/directives.html#table-of-contents>`_
*  `the latest changes <http://docutils.sourceforge.net/docs/ref/rst/>`_
*  `another useful resource <http://www.math.uiuc.edu/~gfrancis/illimath/windows/aszgard_mini/movpy-2.0.0-py2.4.4/manuals/docutils/ref/rst/directives.html>`_
*   `From Python <https://docs.python.org/devguide/documenting.html>`_
*   `FAQ <http://docutils.sourceforge.net/FAQ.html>`_
*  `Restructured Text (reST) and Sphinx CheatSheet <http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html>`_
*  `<http://www.siafoo.net/help/reST>`_

۵. گرچه پروژه تحت وب است و تقریبا همه‌ی ابزارهای به کارگرفته شده در ویندوز هم وجود دارد و کاربر نهایی نیز در ویندوز است ولی برای برنامه نویسی باید در لینوکس کار کنید تا بتوانیم ساده‌تر و به یک شکل کار کنیم و نگران تفاوت‌ها در پیشبرد کار نباشیم و به هیچ عنوان زمانی برای سازگاری برنامه نوشته شده در ویندوز گذاشته نمی‌شود و برنامه نویس کنار گذاشته خواهد شد.

۶. در این پروژه‌ی آزمایشی تا می‌توانید روی بخش‌هایی که تسلط بیشتری دارید کار کنید تا در پروژه‌ی اصلی وظیفه‌ای متناسب‌تر با توانایی شما به شما داده شود.

۷. در پروژه‌ی اصلی به هیچ عنوان همه یا بخشی از پروژه یا حتی توضیح‌های آن را نباید در اختیار هیچ کس بگذارید. دقت کنید که پروژه‌ی اصلی تجاری است و طبیعی است هر گونه کپی برداری از کد یا ایده‌ی آن دردسرساز است.

۸. قانون‌های سبک نگارش یکسانی را باید رعایت کنید که به مروز برایتان می‌نویسم.

۸ـ۱ـ همیشه به جای tab باید دو فاصله گذاشته شود.

۸ـ۲ـ تورفتگی برای بدنه‌ی حلقه‌ها،‌ شرط‌ها و همانند آن باید با دو فاصله انجام شود.


**نحوه نصب پایگاه داده ها در این پروژه:**

اول در ترمینال اگر postgresql را نصب نداریم آن را نصب میکنیم:

   sudo apt-get update
   sudo apt-get install postgresql postgresql-contrib
  

سپس برای اجرای postgres باید psycopg2 را نصب کنیم اما برای نصب psycopg2 باید اول postgresql-server را نصب کنیم.

  sudo apt-get install postgresql-server-dev-9.5 postgresql-server-dev-all
  sudo pip3 install psycopg2
 

حال که همه چیز را نصب کردیم وارد محیط postgres میشویم.

 
  sudo -su postgres
  psql

حال باید پایگاه داده را در آن درست کنیم:
۱)ابتدا در psql دستورات زیر را وارد میکنیم تا پایگاه داده را بسازیم : 

  CREATE USER installer WITH PASSWORD 'abc123';
  CREATE USER visitor WITH PASSWORD 'abc123';
  CREATE USER admin WITH PASSWORD 'abc123';
  CREATE DATABASE cms;
 
۲)سپس این دستورات را وارد میکنیم تا جدول ها را بسازیم : 

 
  CREATE TABLE tbl_callus (id BIGSERIAL PRIMARY KEY NOT NULL,name VARCHAR(50) NOT NULL , mail varchar(100) NOT NULL , msg VARCHAR(300) NOT NULL);
  CREATE TABLE tbl_tags (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(40) NOT NULL );
  CREATE TABLE tbl_articles (id SERIAL NOT NULL PRIMARY KEY , title VARCHAR(50) NOT NULL , keywords VARCHAR(50) NOT NULL ,description VARCHAR(300) NOT NULL , text TEXT NOT NULL,id_tags SERIAL REFERENCES  tbl_tags(id) NOT NULL);
  =======
  CREATE TABLE tbl_callus (id BIGSERIAL PRIMARY KEY NOT NULL,name VARCHAR(50) NOT NULL , mail varchar(100) NOT NULL , msg VARCHAR(300) NOT NULL);
  CREATE TABLE tbl_tags (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(40) NOT NULL );
  CREATE TABLE tbl_articles (id SERIAL NOT NULL PRIMARY KEY , title VARCHAR(50) NOT NULL , keywords VARCHAR(50) NOT NULL ,description VARCHAR(300) NOT NULL , text TEXT NOT NULL,id_tags SERIAL REFERENCES  tbl_tags(id) NOT NULL);
  CREATE TABLE tbl_users (id SERIAL NOT NULL PRIMARY KEY,username varchar(50) NOT NULL,password varchar(50) NOT NULL,salt varchar(50) NOT NULL,cookie varchar(50));


  CREATE TABLE tbl_comments (id SERIAL NOT NULL PRIMARY KEY , name VARCHAR(50) NOT NULL , mail VARCHAR(100) NOT NULL , text VARCHAR(300) NOT NULL , article_id SERIAL NOT NULL REFERENCES tbl_articles(id));

  ALTER TABLE tbl_articles DROP COLUMN id_tags;
 

۳)حال میزان دسترسی کاربران را تعیین میکنیم :

 
  GRANT ALL PRIVILEGES ON DATABASE cms TO installer;
  GRANT SELECT ON tbl_articles,tbl_callus,tbl_tags,tbl_comments TO visitor;
  GRANT INSERT ON tbl_comments TO visitor;
  GRANT SELECT,USAGE ON SEQUENCE tbl_comments_id_seq TO visitor;
  GRANT SELECT,INSERT ON tbl_articles,tbl_callus,tbl_tags TO admin
  GRANT DELETE ON tbl_articles TO admin;
  GRANT DELETE,SELECT ON tbl_comments TO admin;
  GRANT UPDATE ON tbl_users TO admin;
  GRANT SELECT,USAGE ON SEQUENCE tbl_articles_id_tags_seq TO admin;
  GRANT UPDATE ON tbl_articles TO admin;
 

۴)حال از محیط psql خارج شده و در فولدر database این دستور را میزنیم : 

  
  python3 installdb.py
 

۵)حال پایگاه داده آماده است برای استفاده و میتوانیم با دستور زیر پروژه را اجرا کنیم :
  
  python3 main.py
  
