

CREATE USER installer WITH PASSWORD 'abc123';
CREATE USER visitor WITH PASSWORD 'abc123';
CREATE USER admin WITH PASSWORD 'abc123';

CREATE DATABASE cms;


GRANT ALL PRIVILEGES ON DATABASE cms TO installer;
GRANT SELECT ON tbl_articles,tbl_callus,tbl_tags,tbl_comments TO visitor;
GRANT INSERT ON tbl_comments TO visitor;
GRANT SELECT,USAGE ON SEQUENCE tbl_comments_id_seq TO visitor;
GRANT SELECT,INSERT ON tbl_articles,tbl_callus,tbl_tags TO admin;
GRANT DELETE ON tbl_articles TO admin;
GRANT DELETE,SELECT ON tbl_comments TO admin;
GRANT SELECT ON tbl_users TO admin;
GRANT UPDATE ON tbl_users TO admin;
GRANT SELECT,USAGE ON SEQUENCE tbl_articles_id_tags_seq TO admin;
GRANT UPDATE ON tbl_articles TO admin;
GRANT INSERT ON tbl_articles TO admin;




CREATE TABLE tbl_callus (id BIGSERIAL PRIMARY KEY NOT NULL,name VARCHAR(50) NOT NULL , mail varchar(100) NOT NULL , msg VARCHAR(300) NOT NULL);
CREATE TABLE tbl_tags (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(40) NOT NULL );
CREATE TABLE tbl_articles (id SERIAL NOT NULL PRIMARY KEY , title VARCHAR(50) NOT NULL , keywords VARCHAR(50) NOT NULL ,description VARCHAR(300) NOT NULL , text TEXT NOT NULL , id_tags SERIAL REFERENCES tbl_tags(id) NOT NULL);
CREATE TABLE tbl_callus (id BIGSERIAL PRIMARY KEY NOT NULL,name VARCHAR(50) NOT NULL , mail varchar(100) NOT NULL , msg VARCHAR(300) NOT NULL);
CREATE TABLE tbl_tags (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(40) NOT NULL );
CREATE TABLE tbl_articles (id SERIAL NOT NULL PRIMARY KEY , title VARCHAR(50) NOT NULL , keywords VARCHAR(50) NOT NULL ,description VARCHAR(300) NOT NULL , text TEXT NOT NULL,id_tags SERIAL REFERENCES tbl_tags(id) NOT NULL);
CREATE TABLE tbl_users (id SERIAL NOT NULL PRIMARY KEY,username varchar(50) NOT NULL,password varchar(50) NOT NULL,salt varchar(50) NOT NULL,cookie varchar(50));
CREATE TABLE tbl_comments (id SERIAL NOT NULL PRIMARY KEY , name VARCHAR(50) NOT NULL , mail VARCHAR(100) NOT NULL , text VARCHAR(300) NOT NULL , article_id SERIAL NOT NULL REFERENCES tbl_articles(id));

ALTER TABLE tbl_articles DROP COLUMN id_tags;

ALTER TABLE tbl_articles ADD score integer;

insert into tbl_users (username,password,salt)values('ali','15ab8357abeb6eacf1b591a1b5b1aedd','1');


tbl_callus
    id SERIAL
    name VARCHAR(50)
    mail VARCHAR(100)
    msg VARCHAR(300)


tbl_tags
    id SERIAL,
    name VARCHAR(40)
    


tbl_articles
    id SERIAL,
    title VARCHAR(50),
    keywords VARCHAR(50),
    description VARCHAR(300),
    text Text
    id_tags SERIAL REFRENCES tbl_tags(id)

tbl_comments
    id SERIAL,
    name VARCHAR(50),
    mail varchar(100),
    text varchar(300),
    article_id SERIAL


