import psycopg2
class DatabaseVisitor:

    def __init__(self):
        self.__connection = psycopg2.connect(database="cms", user="visitor", password="abc123", host="127.0.0.1", port="5432")
        self.cursor = self.__connection.cursor()
    def __del__(self):
        self.__connection.close()
        self.cursor.close()
    def execute(self,sql,param=[]):
        self.cursor.execute(sql,tuple(param))
        self.__connection.commit();

    def executeFetch(self,sql,param=[]):
        self.cursor.execute(sql,tuple(param))
        rows = self.cursor.fetchall()
        return rows        

class DatabaseAdmin:
    
    def __init__(self):
        self.__connection = psycopg2.connect(database="cms", user="admin", password="abc123", host="127.0.0.1", port="5432")
        self.cursor = self.__connection.cursor()
    def __del__(self):
        self.__connection.close()
        self.cursor.close()
    def execute(self,sql,param=[]):
        self.cursor.execute(sql,tuple(param))
        self.__connection.commit()
        
    def executeFetch(self,sql,param=[]):
        self.cursor.execute(sql,tuple(param))
        rows = self.cursor.fetchall()
        return rows
