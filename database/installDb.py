'''
this python script will Create the required tables

'''
import psycopg2

connection = psycopg2.connect(database="cms", user="installer", password="abc123", host="127.0.0.1", port="5432")
cursor = connection.cursor()
cursor.execute("CREATE TABLE tbl_callus (id BIGSERIAL PRIMARY KEY NOT NULL,name VARCHAR(50) NOT NULL , mail varchar(100) NOT NULL , msg VARCHAR(300) NOT NULL)")
cursor.execute("CREATE TABLE tbl_tags (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(40) NOT NULL )")
cursor.execute("CREATE TABLE tbl_articles (id SERIAL NOT NULL PRIMARY KEY , title VARCHAR(50) NOT NULL , keywords VARCHAR(50) NOT NULL ,description VARCHAR(300) NOT NULL , text TEXT NOT NULL,id_tags SERIAL REFERENCES tbl_tags(id) NOT NULL)")
cursor.execute("CREATE TABLE tbl_comments (id SERIAL NOT NULL PRIMARY KEY , name VARCHAR(50) NOT NULL , mail VARCHAR(100) NOT NULL , text VARCHAR(300) NOT NULL , article_id SERIAL NOT NULL REFERENCES tbl_articles(id));")
connection.close()



