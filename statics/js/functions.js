function Gethtml(u,targetId){
  
  var xmlhttp = new XMLHttpRequest();
  var url = u;
  
  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
          var text =xmlhttp.responseText;
          document.getElementById(targetId).innerHTML = text;
      }
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send(); 

}